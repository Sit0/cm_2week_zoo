package pl.codementors.zoo;


import java.io.*;
import java.util.Scanner;

/**
 * Created by sit0 on 05.06.17.
 */
public class ZooMain {
    /**
     * Application main method.
     *
     * @param args Application starting parameters.
     */
    public static void main(String[] args) {

        Animal[] animals = null;
        Scanner inputScanner = new Scanner(System.in);
        boolean runner = true;

        while (runner) {
            System.out.println("\nZOO MENU:\n1.Dodaj zwierzęta ręcznie" +
                    "\n2.Wczytaj zwierzęta z pliku tektowego" +
                    "\n3.Wczytaj zwierzęta z pliku binarnego" +
                    "\n4.Wypisz wszystkie zwierzęta" +
                    "\n5.Nakarm zwierzątka" +
                    "\n6.Zwierzątka dają głos" +
                    "\n7.Zapisz zwierzęta do pliku tektowego" +
                    "\n8.Zapisz zwierzęta do pliku binarnego" +
                    "\n0.Wyjdź z programu");
            switch (inputScanner.nextInt()) {

                case 1: {
                    System.out.println("Podaj ilość zwierząt");
                    int numberOfAnimals = inputScanner.nextInt();
                    inputScanner.skip("\n");
                    animals = new Animal[numberOfAnimals];
                    for (int i = 0; i < numberOfAnimals; i++) {

                        while (true) {
                            System.out.println("Podaj rodzaj zwierzątka " + (i + 1)
                                    + " (Wolf,Parrot,Iguana) oraz imie oraz kolor zwierzęcia oddzielając spacją ");
                            String animal = inputScanner.next();
                            String name = inputScanner.next();
                            String color = inputScanner.next();
                            inputScanner.skip("\n");
                            if (animal.equals("Wolf")) {
                                Wolf myWolf = new Wolf();
                                myWolf.setName(name);
                                myWolf.setFurColor(color);
                                animals[i] = myWolf;
                                break;
                            }
                            if (animal.equals("Parrot")) {
                                Parrot myParrot = new Parrot();
                                myParrot.setName(name);
                                myParrot.setFeatherColor(color);
                                animals[i] = myParrot;
                                break;
                            }
                            if (animal.equals("Iguana")) {
                                Iguana myIguana = new Iguana();
                                myIguana.setName(name);
                                myIguana.setScaleColor(color);
                                animals[i] = myIguana;
                                break;
                            } else {
                                System.out.println("Nie wiem co to " + animal + ". Podaj poprawną nazwe zwierzęcia raz jeszcze");
                            }
                        }
                    }
                    System.out.println("Dodano zwierzęta");
                    break;
                }

                case 2: {
                    animals = readFromFile("zoo.save");
                    System.out.println("Wczytano plik");
                    break;
                }

                case 3: {
                    animals = readFromBinaryFile("zoo.save.bin");
                    System.out.println("Wczytano plik");
                    break;
                }
                case 4: {
                    if (animals != null) {
                        print(animals);
                    } else {
                        System.out.println("W Zoo nie ma żadnych zwierząt");
                    }
                    break;
                }

                case 5: {
                    if (animals != null) {
                        feed(animals);
                    } else {
                        System.out.println("W Zoo nie ma żadnych zwierząt");
                    }
                    break;
                }

                case 6: {
                    if (animals != null) {
                        speak(animals);
                    } else {
                        System.out.println("W Zoo nie ma żadnych zwierząt");
                    }
                    break;
                }

                case 7: {
                    saveToFile(animals, "zoo.save");
                    System.out.println("Zapisano plik");
                    break;
                }

                case 8: {
                    saveToBinaryFile(animals, "zoo.save.bin");
                    System.out.println("Zapisano plik");
                    break;
                }

                case 0: {
                    runner = false;
                    System.out.println("EXIT, do zobaczenia");
                    break;
                }

                default: {
                    System.out.println("Nie ma takiej pozycji w MENU. Wpisz raz jeszcze");
                }
            }
        }
    }

    /**
     * Print on console Class(as String), name and color of each animal
     *
     * @param animals Array of animals
     */
    static void print(Animal[] animals) {
        System.out.println("Zwierzęta w klatakch:\n");
        System.out.println("------------------");

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Wolf) {
                System.out.println("Klatka " + (i + 1) + "\nTyp: Wolf\n" + "Imię: "
                        + animals[i].getName() + "\nKolor: " + ((Wolf) animals[i]).getFurColor());
                System.out.println("------------------");
            }
            if (animals[i] instanceof Parrot) {
                System.out.println("Klatka " + (i + 1) + "\nTyp: Parrot\n" + "Imię: "
                        + animals[i].getName() + "\nKolor: " + ((Parrot) animals[i]).getFeatherColor());
                System.out.println("------------------");
            }
            if (animals[i] instanceof Iguana) {
                System.out.println("Klatka " + (i + 1) + "\nTyp: Iguana\n" + "Imię: "
                        + animals[i].getName() + "\nKolor: " + ((Iguana) animals[i]).getScaleColor());
                System.out.println("------------------");
            }
        }
    }

    /**
     * Write animals into text file. First line is number of animals,
     * than Class Name Color for each animal
     *
     * @param animals Array of animals
     * @param file    Name of save file (could be with path)
     */
    static void saveToFile(Animal[] animals, String file) {
        System.out.println("Zapisuje do pliku " + file);
        try (FileWriter fw = new FileWriter(file);
             BufferedWriter bw = new BufferedWriter(fw);) {
            int count = 0;
            for (Animal i : animals) {
                count++;
            }
            bw.write(count + "\n");
            for (Animal i : animals) {
                String animalClass = i.getClass().getSimpleName();
                bw.write(animalClass + " ");
                bw.write(i.getName() + " ");
                if (animalClass.equals("Wolf")) {
                    bw.write(((Wolf) i).getFurColor() + "\n");
                }
                if (animalClass.equals("Parrot")) {
                    bw.write(((Parrot) i).getFeatherColor() + "\n");
                }
                if (animalClass.equals("Iguana")) {
                    bw.write(((Iguana) i).getScaleColor() + "\n");
                }
            }
        } catch (IOException ex) {
            System.out.println("Problem z zapisem");
        }
    }

    /**
     * Read animals from text into Animal array.
     *
     * @param file Name of file to read (could be with path)
     * @return Array of animals
     */
    static Animal[] readFromFile(String file) {
        System.out.println("Czytam z pliku " + file);
        Animal[] readAnimals;
        try (FileReader fr = new FileReader(file);
             Scanner reader = new Scanner(fr);) {
            int count = reader.nextInt();
            readAnimals = new Animal[count];
            for (int i = 0; i < count; i++) {
                String animalClass = reader.next();
                if (animalClass.equals("Wolf")) {
                    Wolf myWolf = new Wolf();
                    myWolf.setName(reader.next());
                    myWolf.setFurColor(reader.next());
                    readAnimals[i] = myWolf;
                }
                if (animalClass.equals("Parrot")) {
                    Parrot myParrot = new Parrot();
                    myParrot.setName(reader.next());
                    myParrot.setFeatherColor(reader.next());
                    readAnimals[i] = myParrot;

                }
                if (animalClass.equals("Iguana")) {
                    Iguana myIguana = new Iguana();
                    myIguana.setName(reader.next());
                    myIguana.setScaleColor(reader.next());
                    readAnimals[i] = myIguana;
                }
            }

        } catch (IOException ex) {

            System.out.println("Problem z wczytaniem");
            return null;
        }

        return readAnimals;
    }

    /**
     * Write animals into binary file.
     *
     * @param animals Array of animals
     * @param file    Name of save file (could be with path)
     */
    static void saveToBinaryFile(Animal[] animals, String file) {
        System.out.println("Zapisuje do pliku " + file);
        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos);) {

            oos.writeObject(animals);

        } catch (IOException ex) {
            System.out.println("Problem z zapisem binarnym");
        }

    }

    /**
     * Read animals from binary file and return Animal array Object.
     *
     * @param file Name of file to read (could be with path)
     * @return Animal array Object
     */
    static Animal[] readFromBinaryFile(String file) {
        System.out.println("Czytam z pliku " + file);
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis);) {

            return (Animal[]) ois.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Problem z odczytem binarnym");
            return null;
        }
    }

    /**
     * Calls appropriate eating method depends if animal is carnivorous or herbivorous.
     *
     * @param animals Animal array
     */
    static void feed(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Carnivorous) {
                ((Carnivorous) i).eatMeat();
            }
            if (i instanceof Herbivorous) {
                ((Herbivorous) i).eatPlant();
            }
        }
    }

    /**
     * Calls appropriate speaking method for each animal.
     *
     * @param animals Animal array
     */
    static void speak(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Wolf) {
                ((Wolf) i).howl();
            }
            if (i instanceof Parrot) {
                ((Parrot) i).screech();
            }
            if (i instanceof Iguana) {
                ((Iguana) i).hiss();
            }
        }
    }

    //Not used methods anymore (functionality added to different methods)
    static void howl(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Wolf) {
                ((Wolf) i).howl();
            }
        }
    }

    static void hiss(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Iguana) {
                ((Iguana) i).hiss();
            }
        }
    }

    static void screech(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Parrot) {
                ((Parrot) i).screech();
            }
        }
    }

    static void feedWithMeat(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Carnivorous) {
                ((Carnivorous) i).eatMeat();
            }
        }
    }

    static void feedWithPlant(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Herbivorous) {
                ((Herbivorous) i).eatPlant();
            }
        }
    }

}
