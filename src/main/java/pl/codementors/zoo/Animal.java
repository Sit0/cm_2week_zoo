package pl.codementors.zoo;

import java.io.Serializable;

/**
 * Created by sit0 on 05.06.17.
 */
public abstract class Animal implements Serializable {
    private String name;
    private int age;

    abstract void eat();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
