package pl.codementors.zoo;

import java.io.Serializable;

/**
 * Created by sit0 on 05.06.17.
 */
public class Iguana extends Lizard implements Herbivorous, Serializable {

    public void hiss(){
        System.out.println(getName() + " is hissing");
    }
    @Override
    public void eat(){
        System.out.println(getName() + " is eating");
    }
    @Override
    public void eatPlant() {
        System.out.println(getName() + " is eating plant");
    }
}
