package pl.codementors.zoo;

import java.io.Serializable;

/**
 * Created by sit0 on 05.06.17.
 */
public abstract class Mammal extends Animal implements Serializable {

    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
