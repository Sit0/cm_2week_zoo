package pl.codementors.zoo;

import java.io.Serializable;

/**
 * Created by sit0 on 05.06.17.
 */
public class Wolf extends Mammal implements Carnivorous, Serializable {

    public void howl(){
        System.out.println(getName() + " is howling");
    }
    @Override
    public void eat(){
        System.out.println(getName() + " is eating");
    }
    @Override
    public void eatMeat() {
        System.out.println(getName() + " is eating meat");
    }
}
