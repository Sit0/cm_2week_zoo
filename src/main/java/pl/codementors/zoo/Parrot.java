package pl.codementors.zoo;

import java.io.Serializable;

/**
 * Created by sit0 on 05.06.17.
 */
public class Parrot extends Bird implements Herbivorous, Serializable {

    public void screech(){
        System.out.println(getName() + " is screeching");
    }
    @Override
    public void eat(){
        System.out.println(getName() + " is eating");
    }
    @Override
    public void eatPlant() {
        System.out.println(getName() + " is eating plant");
    }
}
