package pl.codementors.zoo;

/**
 * Created by sit0 on 06.06.17.
 */
public interface Carnivorous {
    void eatMeat();
}
